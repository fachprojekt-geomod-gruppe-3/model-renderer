Simulates and renders the processing of a workpiece according to tool commands.

Takes arguments of the form "filepath number\[:number\] number\[:number\] number\[:number\]", where the filepath points to a CSV file containing the tool commands (as outputted by the other project) and the numbers specify the lengths and optionally resolutions of the workpiece in three dimensions. If a resolution is not explicitly provided, it is set to the same value as the corresponding length, which is equivalent to one simulated unit per millimeter.

Controls:
- WASD / arrow keys: Translate camera in corresponding direction.
- Mouse scrolling: Translate camera forwards or backwards.
- Mouse scrolling + SHIFT: Zoom in or out.
- Mouse movement: Look around.
- R: Reset camera to initial state.
- E: Toggle skip mode.
- SPACE: Toggle pause mode.
- ESCAPE: Close program.

Notes:
- The program is initially in pause mode. Press SPACE to start the simulation.
- The mouse controls should instead also work with most touchpads.
- The program currently only supports a Voxel model of the workpiece.
