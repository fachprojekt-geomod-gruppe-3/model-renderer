use std::{sync::Arc, fmt};
use nalgebra_glm as glm;

pub trait Csg: fmt::Debug {
    fn contains_point(&self, position: glm::Vec3, point: glm::Vec3) -> bool;

    fn bounding_box(&self) -> Option<BoundingBox>;
}

#[derive(Debug, Clone, Copy)]
pub struct Sphere {
    radius: f32,
}

impl Sphere {
    pub fn new(radius: f32) -> Sphere {
        assert!(radius.is_normal() && radius.is_sign_positive(), "Invalid radius.");

        Sphere { radius }
    }
}

impl Csg for Sphere {
    fn contains_point(&self, position: glm::Vec3, point: glm::Vec3) -> bool {
        glm::length(&(point - position)) <= self.radius
    }

    fn bounding_box(&self) -> Option<BoundingBox> {
        let max_corner = glm::vec3(self.radius, self.radius, self.radius);
        let min_corner = -max_corner;

        Some(BoundingBox { min_corner, max_corner })
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Cuboid {
    max_corner: glm::Vec3,
}

impl Cuboid {
    pub fn new(x_length: f32, y_length: f32, z_length: f32) -> Cuboid {
        assert!(x_length.is_normal() && x_length.is_sign_positive(), "Invalid x length.");
        assert!(y_length.is_normal() && y_length.is_sign_positive(), "Invalid y length.");
        assert!(z_length.is_normal() && z_length.is_sign_positive(), "Invalid z length.");

        let max_corner = glm::vec3(x_length / 2.0, y_length / 2.0, z_length / 2.0);

        Cuboid { max_corner }
    }
}

impl Csg for Cuboid {
    fn contains_point(&self, position: glm::Vec3, point: glm::Vec3) -> bool {
        let offset = point - position;

        glm::all(&glm::less_than_equal(&glm::abs(&offset), &self.max_corner))
    }

    fn bounding_box(&self) -> Option<BoundingBox> {
        let max_corner = self.max_corner;
        let min_corner = -max_corner;

        Some(BoundingBox { min_corner, max_corner })
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Cylinder {
    radius: f32,
    half_length: f32,
    axis: Axis,
}

impl Cylinder {
    pub fn new(radius: f32, length: f32, axis: Axis) -> Cylinder {
        assert!(radius.is_normal() && radius.is_sign_positive(), "Invalid radius.");
        assert!(length.is_normal() && length.is_sign_positive(), "Invalid length.");

        let half_length = length / 2.0;

        Cylinder { radius, half_length, axis }
    }
}

impl Csg for Cylinder {
    fn contains_point(&self, position: glm::Vec3, point: glm::Vec3) -> bool {
        let offset = point - position;

        match self.axis {
            Axis::X => offset.x.abs() <= self.half_length && glm::length(&offset.yz()) <= self.radius,
            Axis::Y => offset.y.abs() <= self.half_length && glm::length(&offset.xz()) <= self.radius,
            Axis::Z => offset.z.abs() <= self.half_length && glm::length(&offset.xy()) <= self.radius,
        }
    }

    fn bounding_box(&self) -> Option<BoundingBox> {
        let max_corner = match self.axis {
            Axis::X => glm::vec3(self.half_length, self.radius, self.radius),
            Axis::Y => glm::vec3(self.radius, self.half_length, self.radius),
            Axis::Z => glm::vec3(self.radius, self.radius, self.half_length),
        };
        let min_corner = -max_corner;

        Some(BoundingBox { min_corner, max_corner })
    }
}

#[derive(Debug, Clone)]
pub struct Compound<T, U> where T: Csg + ?Sized, U: Csg + ?Sized {
    base: Arc<T>,
    other: Arc<U>,
    offset: glm::Vec3,
    compound_type: CompoundType,
    bounding_box: Option<BoundingBox>,
}

impl<T, U> Compound<T, U> where T: Csg + ?Sized, U: Csg + ?Sized {
    fn new(base: Arc<T>, other: Arc<U>, offset: glm::Vec3, compound_type: CompoundType) -> Compound<T, U> {
        let bounding_box = match (base.bounding_box(), other.bounding_box(), compound_type) {
            (Some(bb), None, CompoundType::Union) | (None, Some(bb), CompoundType::Union) => Some(bb),
            (Some(bb1), Some(bb2), CompoundType::Union) => Some(bb1.union(&bb2, offset)),
            (Some(bb1), Some(bb2), CompoundType::Intersection) => bb1.intersection(&bb2, offset),
            (bb_opt, _, CompoundType::Difference) => bb_opt,
            _ => None,
        };

        Compound {
            base,
            other,
            offset,
            compound_type,
            bounding_box,
        }
    }

    pub fn union(base: Arc<T>, other: Arc<U>, offset: glm::Vec3) -> Compound<T, U> {
        Compound::new(base, other, offset, CompoundType::Union)
    }

    pub fn intersection(base: Arc<T>, other: Arc<U>, offset: glm::Vec3) -> Compound<T, U> {
        Compound::new(base, other, offset, CompoundType::Intersection)
    }

    pub fn difference(base: Arc<T>, other: Arc<U>, offset: glm::Vec3) -> Compound<T, U> {
        Compound::new(base, other, offset, CompoundType::Difference)
    }
}

impl<T, U> Csg for Compound<T, U> where T: Csg + ?Sized, U: Csg + ?Sized {
    fn contains_point(&self, position: glm::Vec3, point: glm::Vec3) -> bool {
        if self.bounding_box().is_none() {
            return false;
        }

        let base_contains = || self.base.contains_point(position, point);
        let other_contains = || self.other.contains_point(position + self.offset, point);

        match self.compound_type {
            CompoundType::Union => base_contains() || other_contains(),
            CompoundType::Intersection => base_contains() && other_contains(),
            CompoundType::Difference => base_contains() && !other_contains(),
        }
    }

    fn bounding_box(&self) -> Option<BoundingBox> {
        self.bounding_box
    }
}

#[derive(Debug, Clone, Copy)]
pub struct BoundingBox {
    min_corner: glm::Vec3,
    max_corner: glm::Vec3,
}

impl BoundingBox {
    fn union(&self, other: &BoundingBox, offset: glm::Vec3) -> BoundingBox {
        let min_corner = glm::min2(&self.min_corner, &(other.min_corner + offset));
        let max_corner = glm::max2(&self.max_corner, &(other.max_corner + offset));

        BoundingBox { min_corner, max_corner }
    }

    fn intersection(&self, other: &BoundingBox, offset: glm::Vec3) -> Option<BoundingBox> {
        let min_corner = glm::max2(&self.min_corner, &(other.min_corner + offset));
        let max_corner = glm::min2(&self.max_corner, &(other.max_corner + offset));

        if glm::all(&glm::less_than_equal(&min_corner, &max_corner)) {
            Some(BoundingBox { min_corner, max_corner })
        } else {
            None
        }
    }

    pub fn x_bounds(&self, position: glm::Vec3) -> (f32, f32) {
        (position.x + self.min_corner.x, position.x + self.max_corner.x)
    }

    pub fn y_bounds(&self, position: glm::Vec3) -> (f32, f32) {
        (position.y + self.min_corner.y, position.y + self.max_corner.y)
    }

    pub fn z_bounds(&self, position: glm::Vec3) -> (f32, f32) {
        (position.z + self.min_corner.z, position.z + self.max_corner.z)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Axis {
    X,
    Y,
    Z,
}

#[derive(Debug, Clone, Copy)]
enum CompoundType {
    Union,
    Intersection,
    Difference,
}
