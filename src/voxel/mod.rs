use crate::{machining::*, camera::Camera, csg::{Csg, Compound}};
use std::{sync::Arc, iter::{successors, Successors}};
use luminance::{
    tess::{Tess, TessBuilder, Mode, TessSliceIndex}, render_state::RenderState, pipeline::PipelineState,
    shader::program::{Program, ProgramInterface, Uniform}, context::GraphicsContext, linear::M44,
};
use luminance_derive::{Semantics, Vertex, UniformInterface};
use luminance_glutin::{GlutinSurface, GlutinError};
use glutin::Event;
use nalgebra_glm as glm;
use crossbeam_channel::{Sender, Receiver, bounded};

pub struct VoxelRenderer {
    surface: GlutinSurface,
    model: VoxelModel,
}

impl VoxelRenderer {
    pub fn new<I>(mut surface: GlutinSurface, workpiece: Workpiece, tools: Vec<Tool>, commands: I)
    -> Result<VoxelRenderer, GlutinError> where I: IntoIterator<Item = Command> + Send + 'static {
        VoxelModel::new(&mut surface, workpiece, tools, commands).map(|model| {
            VoxelRenderer { surface, model }
        })
    }

    pub fn update_model(&mut self, wait: bool) {
        self.model.update(&mut self.surface, wait);
    }

    pub fn render(&mut self, camera: &Camera) -> Result<(), GlutinError> {
        self.model.render(&mut self.surface, camera)
    }

    pub fn poll_surface_events<F>(&mut self, f: F) where F: FnMut(Event) {
        self.surface.event_loop.poll_events(f)
    }
}

struct VoxelModel {
    instance_manager: InstanceManager,
    mesh: Tess,
    receiver: Option<Receiver<Message>>,
    command_thread_handle: Option<std::thread::JoinHandle<()>>,
    model_state: ModelState,
    pipeline_state: PipelineState,
    render_state: RenderState,
    program: Program<VoxelSemantics, (), ShaderInterface>,
}

impl VoxelModel {
    fn new<I>(surface: &mut GlutinSurface, workpiece: Workpiece, tools: Vec<Tool>, commands: I)
    -> Result<VoxelModel, GlutinError> where I: IntoIterator<Item = Command> + Send + 'static {
        let back_buffer = surface.back_buffer()?;
        let pipeline_state = PipelineState::new().set_clear_color(CLEAR_COLOUR);
        let render_state = RenderState::default();
        let program = Program::from_strings(None, VERTEX_SHADER, None, FRAGMENT_SHADER)
            .expect("Failed building shader program.").ignore_warnings();

        surface.pipeline_builder().pipeline(&back_buffer, &pipeline_state, |_, mut shd_gate| {
            shd_gate.shade(&program, |iface: ProgramInterface<ShaderInterface>, _| {
                iface.scalings.update(workpiece.ratios().into());
            });
        });

        let mut instance_manager = InstanceManager::new(workpiece);
        let mesh = instance_manager.create_mesh(surface);
        let (sender, receiver) = bounded(CHANNEL_CAPACITY);

        let command_thread_handle = Some(spawn_command_thread(workpiece, tools, commands, sender));

        Ok(VoxelModel {
            instance_manager,
            mesh,
            receiver: Some(receiver),
            command_thread_handle,
            model_state: ModelState::Unchanged,
            pipeline_state,
            render_state,
            program,
        })
    }

    fn update(&mut self, surface: &mut GlutinSurface, wait: bool) {
        if self.model_state == ModelState::Unchanged || self.model_state == ModelState::PendingChanges {
            if let Some(ref receiver) = self.receiver {
                for _ in 0..CHANNEL_CAPACITY {
                    match receiver.try_recv() {
                        Ok(Message::DestroyVoxel(point)) => {
                            if self.instance_manager.destroy_voxel(point) {
                                self.model_state = ModelState::PendingChanges;
                            }
                        },
                        Ok(Message::PrepareRender) => {
                            if wait && self.model_state == ModelState::PendingChanges {
                                self.mesh = self.instance_manager.create_mesh(surface);
                                self.model_state = ModelState::WaitingForRender;
                                break;
                            }
                        },
                        Err(e) => {
                            if e.is_disconnected() {
                                self.mesh = self.instance_manager.create_mesh(surface);
                                self.model_state = ModelState::Finished;
                                eprintln!("Model processing simulation finished.");
                            }

                            break;
                        },
                    }
                }
            }
        }
    }

    fn render(&mut self, surface: &mut GlutinSurface, camera: &Camera) -> Result<(), GlutinError> {
        let back_buffer = surface.back_buffer()?;
        let projection = camera.projection(surface).into();
        let view = camera.view().into();

        surface.pipeline_builder().pipeline(&back_buffer, &self.pipeline_state, |_, mut shd_gate| {
            shd_gate.shade(&self.program, |iface, mut rdr_gate| {
                iface.projection.update(projection);
                iface.view.update(view);

                rdr_gate.render(&self.render_state, |mut tess_gate| {
                    tess_gate.render(self.mesh.slice(..));
                });
            });
        });

        surface.swap_buffers();
        
        if self.model_state == ModelState::WaitingForRender {
            self.model_state = ModelState::Unchanged;
        }

        Ok(())
    }
}

impl Drop for VoxelModel {
    fn drop(&mut self) {
        if self.model_state != ModelState::Finished {
            eprintln!("Model processing simulation aborted.");
        }

        self.receiver = None;

        if let Some(handle) = self.command_thread_handle.take() {
            let join_result = handle.join();

            if !std::thread::panicking() {
                join_result.unwrap();
            }
        }
    }
}

#[derive(Debug)]
struct InstanceManager {
    instance_states: Vec<InstanceState>,
    instance_buffer: Vec<VoxelInstance>,
    workpiece: Workpiece,
}

impl InstanceManager {
    fn new(workpiece: Workpiece) -> InstanceManager {
        let [x_res, y_res, z_res]: [usize; 3] = workpiece.resolutions().into();
        let instance_count = x_res * y_res * z_res;
        let mut instance_states = Vec::with_capacity(instance_count);
        let instance_buffer = Vec::with_capacity(instance_count);

        for x in 0..x_res {
            for y in 0..y_res {
                for z in 0..z_res {
                    let instance = VoxelInstance::new(
                        InstancePosition::new([
                            x as f32 * workpiece.ratios().x,
                            y as f32 * workpiece.ratios().y,
                            -(z as f32 * workpiece.ratios().z),
                        ])
                    );

                    let visible = x == 0 || y == 0 || z == 0 || x == x_res - 1 ||
                        y == y_res - 1 || z == z_res - 1;

                    instance_states.push(InstanceState::Alive { instance, visible });
                }
            }
        }

        InstanceManager { instance_states, instance_buffer, workpiece }
    }

    fn create_mesh(&mut self, surface: &mut GlutinSurface) -> Tess {
        self.instance_buffer.clear();

        self.instance_buffer.extend(self.instance_states.iter().filter_map(|state| {
            match state {
                InstanceState::Alive { instance, visible } if *visible => Some(instance),
                _ => None
            }
        }).cloned());

        TessBuilder::new(surface)
            .add_instances(&self.instance_buffer)
            .add_vertices(VOXEL_VERTICES)
            .set_indices(VOXEL_INDICES)
            .set_primitive_restart_index(Some(RESTART))
            .set_mode(Mode::TriangleStrip)
            .build()
            .expect("Failed creating mesh.")
    }

    fn destroy_voxel(&mut self, point: glm::Vec3) -> bool {
        let x = (point.x / self.workpiece.ratios().x).floor() as usize;
        let y = (point.y / self.workpiece.ratios().y).floor() as usize;
        let z = (-point.z / self.workpiece.ratios().z).floor() as usize;
        let index = self.index_from_coords(x, y, z);

        match std::mem::replace(&mut self.instance_states[index], InstanceState::Destroyed) {
            InstanceState::Destroyed => false,
            InstanceState::Alive { .. } => {
                let [x_res, y_res, z_res]: [usize; 3] = self.workpiece.resolutions().into();
                let neighbour_coords = [
                    (x.saturating_sub(1), y, z),
                    (x + 1, y, z),
                    (x, y.saturating_sub(1), z),
                    (x, y + 1, z),
                    (x, y, z.saturating_sub(1)),
                    (x, y, z + 1),
                ];

                for &(x,y,z) in &neighbour_coords {
                    if x > 0 && y > 0 && z > 0 && x < x_res - 1 && y < y_res - 1 && z < z_res - 1 {
                        let index = self.index_from_coords(x, y, z);
                        self.instance_states[index].make_visible();
                    }
                }
    
                true
            }
        }
    }

    fn index_from_coords(&self, x: usize, y: usize, z: usize) -> usize {
        let [_, y_res, z_res]: [usize; 3] = self.workpiece.resolutions().into();

        x * y_res * z_res + y * z_res + z
    }
}

#[derive(Debug, Clone, Copy)]
enum Message {
    DestroyVoxel(glm::Vec3),
    PrepareRender,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum ModelState {
    Unchanged,
    PendingChanges,
    WaitingForRender,
    Finished,
}

#[derive(Debug, Clone, Copy)]
enum InstanceState {
    Alive { instance: VoxelInstance, visible: bool },
    Destroyed,
}

impl InstanceState {
    fn make_visible(&mut self) {
        if let InstanceState::Alive { visible, .. } = self {
            *visible = true;
        }
    }
}

#[derive(Debug, Clone, Copy, Semantics)]
pub enum VoxelSemantics {
    #[sem(name = "instance_position", repr = "[f32; 3]", wrapper = "InstancePosition")]
    InstancePosition,
    #[sem(name = "vertex_offset", repr = "[f32; 3]", wrapper = "VertexOffset")]
    VertexOffset,
    #[sem(name = "vertex_normal", repr = "[f32; 3]", wrapper = "VertexNormal")]
    VertexNormal,
}

#[derive(Debug, Clone, Copy, Vertex)]
#[vertex(sem = "VoxelSemantics", instanced = "true")]
struct VoxelInstance {
    position: InstancePosition,
}

#[derive(Debug, Clone, Copy, Vertex)]
#[vertex(sem = "VoxelSemantics")]
struct VoxelVertex {
    offset: VertexOffset,
    normal: VertexNormal,
}

#[derive(Debug, UniformInterface)]
struct ShaderInterface {
    #[uniform(unbound)]
    projection: Uniform<M44>,
    #[uniform(unbound)]
    view: Uniform<M44>,
    #[uniform(unbound)]
    scalings: Uniform<[f32; 3]>,
}

fn spawn_command_thread<I>(workpiece: Workpiece, tools: Vec<Tool>, commands: I, sender: Sender<Message>)
-> std::thread::JoinHandle<()> where I: IntoIterator<Item = Command> + Send + 'static {
    std::thread::spawn(move || {
        let mut current_tool_opt = None;
        let workpiece_csg_model = Arc::new(workpiece.as_cuboid());
        let workpiece_midpoint = glm::vec3(
            workpiece.lengths().x as f32 / 2.0,
            workpiece.lengths().y as f32 / 2.0,
            -(workpiece.lengths().z as f32 / 2.0),
        );

        for command in commands.into_iter() {
            match (command, current_tool_opt) {
                (Command::ToolCall { number }, _) => {
                    current_tool_opt = tools.get(number - 1);

                    if current_tool_opt.is_none() {
                        eprintln!("Warning: Unknown tool no. {} is called by a command.", number);
                    }
                },
                (Command::Movement { .. }, None) => { /* Do nothing. */ },
                (Command::Movement { target, .. }, Some(tool)) => {
                    let position = target + tool.position_offset();
                    let workpiece_offset = workpiece_midpoint - position;

                    let intersection = Compound::intersection(
                        Arc::clone(tool.csg_model()),
                        Arc::clone(&workpiece_csg_model),
                        workpiece_offset,
                    );

                    if let Some(bb) = intersection.bounding_box() {
                        for x in steps(bb.x_bounds(position), workpiece.ratios().x) {
                            for y in steps(bb.y_bounds(position), workpiece.ratios().y) {
                                for z in steps(bb.z_bounds(position), workpiece.ratios().z) {
                                    let point = glm::vec3(x, y, z);

                                    if intersection.contains_point(position, point) {
                                        let send_result = sender.send(Message::DestroyVoxel(point));

                                        if send_result.is_err() {
                                            return;
                                        }
                                    }
                                }
                            }
                        }

                        let send_result = sender.send(Message::PrepareRender);
                        
                        if send_result.is_err() {
                            return;
                        }
                    }
                }
            }
        }
    })
}

fn steps((min, max): (f32, f32), step: f32) -> Successors<f32, impl FnMut(&f32) -> Option<f32>> {
    let start_candidate = step * (min / step).ceil() - step / 2.0;
    let start = if start_candidate >= min { start_candidate } else { start_candidate + step };

    successors(Some(start).filter(|&value| value <= max), move |current| {
        Some(current + step).filter(|&value| value <= max)
    })
}

const LEFT_FRONT_TOP    : VertexOffset = VertexOffset::new([ 0.0, 0.0,  0.0]);
const LEFT_FRONT_BOTTOM : VertexOffset = VertexOffset::new([ 0.0, 0.0, -1.0]);
const LEFT_BACK_TOP     : VertexOffset = VertexOffset::new([ 0.0, 1.0,  0.0]);
const LEFT_BACK_BOTTOM  : VertexOffset = VertexOffset::new([ 0.0, 1.0, -1.0]);
const RIGHT_FRONT_TOP   : VertexOffset = VertexOffset::new([ 1.0, 0.0,  0.0]);
const RIGHT_FRONT_BOTTOM: VertexOffset = VertexOffset::new([ 1.0, 0.0, -1.0]);
const RIGHT_BACK_TOP    : VertexOffset = VertexOffset::new([ 1.0, 1.0,  0.0]);
const RIGHT_BACK_BOTTOM : VertexOffset = VertexOffset::new([ 1.0, 1.0, -1.0]);

const FACING_LEFT  : VertexNormal = VertexNormal::new([-1.0,  0.0,  0.0]);
const FACING_RIGHT : VertexNormal = VertexNormal::new([ 1.0,  0.0,  0.0]);
const FACING_FRONT : VertexNormal = VertexNormal::new([ 0.0, -1.0,  0.0]);
const FACING_BACK  : VertexNormal = VertexNormal::new([ 0.0,  1.0,  0.0]);
const FACING_BOTTOM: VertexNormal = VertexNormal::new([ 0.0,  0.0, -1.0]);
const FACING_TOP   : VertexNormal = VertexNormal::new([ 0.0,  0.0,  1.0]);

const VOXEL_VERTICES: [VoxelVertex; 24] = [
    VoxelVertex::new(LEFT_FRONT_TOP, FACING_LEFT),
    VoxelVertex::new(LEFT_FRONT_TOP, FACING_FRONT),
    VoxelVertex::new(LEFT_FRONT_TOP, FACING_TOP),

    VoxelVertex::new(LEFT_FRONT_BOTTOM, FACING_LEFT),
    VoxelVertex::new(LEFT_FRONT_BOTTOM, FACING_FRONT),
    VoxelVertex::new(LEFT_FRONT_BOTTOM, FACING_BOTTOM),

    VoxelVertex::new(LEFT_BACK_TOP, FACING_LEFT),
    VoxelVertex::new(LEFT_BACK_TOP, FACING_BACK),
    VoxelVertex::new(LEFT_BACK_TOP, FACING_TOP),

    VoxelVertex::new(LEFT_BACK_BOTTOM, FACING_LEFT),
    VoxelVertex::new(LEFT_BACK_BOTTOM, FACING_BACK),
    VoxelVertex::new(LEFT_BACK_BOTTOM, FACING_BOTTOM),

    VoxelVertex::new(RIGHT_FRONT_TOP, FACING_RIGHT),
    VoxelVertex::new(RIGHT_FRONT_TOP, FACING_FRONT),
    VoxelVertex::new(RIGHT_FRONT_TOP, FACING_TOP),

    VoxelVertex::new(RIGHT_FRONT_BOTTOM, FACING_RIGHT),
    VoxelVertex::new(RIGHT_FRONT_BOTTOM, FACING_FRONT),
    VoxelVertex::new(RIGHT_FRONT_BOTTOM, FACING_BOTTOM),

    VoxelVertex::new(RIGHT_BACK_TOP, FACING_RIGHT),
    VoxelVertex::new(RIGHT_BACK_TOP, FACING_BACK),
    VoxelVertex::new(RIGHT_BACK_TOP, FACING_TOP),

    VoxelVertex::new(RIGHT_BACK_BOTTOM, FACING_RIGHT),
    VoxelVertex::new(RIGHT_BACK_BOTTOM, FACING_BACK),
    VoxelVertex::new(RIGHT_BACK_BOTTOM, FACING_BOTTOM),
];

const RESTART: u32 = 25;
const VOXEL_INDICES: [u32; 29] = [
    0, 3, 6, 9,     // Left face
    RESTART,
    1, 4, 13, 16,   // Front face
    RESTART,
    2, 8, 14, 20,   // Top face
    RESTART,
    5, 11, 17, 23,  // Bottom face
    RESTART,
    7, 10, 19, 22,  // Back face
    RESTART,
    12, 15, 18, 21, // Right face
];

const VERTEX_SHADER: &str = include_str!("vs.glsl");
const FRAGMENT_SHADER: &str = include_str!("fs.glsl");
const CLEAR_COLOUR: [f32; 4] = [242.0 / 255.0, 242.0 / 255.0, 242.0 / 255.0, 1.0];
const CHANNEL_CAPACITY: usize = 10_000_000;
