in vec3 fragment_normal;

out vec3 fragment_colour;

void main() {
    vec3 colour = vec3(0.6, 0.6, 0.6);
    vec3 light_direction = vec3(1.0, 0.67, -0.55);
    float diffusion = dot(fragment_normal, -light_direction);

    fragment_colour = diffusion * colour;
}
