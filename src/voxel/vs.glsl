in vec3 instance_position;
in vec3 vertex_offset;
in vec3 vertex_normal;

out vec3 fragment_normal;

uniform mat4 projection;
uniform mat4 view;
uniform vec3 scalings;

void main() {
    vec3 vertex_position = instance_position + scalings * vertex_offset;
    
    gl_Position = projection * view * vec4(vertex_position, 1.0);
    fragment_normal = vertex_normal;
}
