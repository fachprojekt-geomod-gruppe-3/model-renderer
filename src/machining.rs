use crate::csg::{Csg, Cuboid};
use std::sync::Arc;
use nalgebra_glm as glm;

#[derive(Debug, Clone, Copy)]
pub struct Workpiece {
    lengths: glm::TVec3<usize>,
    resolutions: glm::TVec3<usize>,
    ratios: glm::Vec3,
}

impl Workpiece {
    pub fn new(lengths: glm::TVec3<usize>, resolutions: glm::TVec3<usize>) -> Workpiece {
        assert!(lengths.x > 0 && lengths.y > 0 && lengths.z > 0, "Invalid lengths.");
        assert!(resolutions.x > 0 && resolutions.y > 0 && resolutions.z > 0, "Invalid resolutions.");

        let ratios = glm::vec3(
            lengths.x as f32 / resolutions.x as f32,
            lengths.y as f32 / resolutions.y as f32,
            lengths.z as f32 / resolutions.z as f32,
        );

        Workpiece { lengths, resolutions, ratios }
    }

    pub fn lengths(&self) -> glm::TVec3<usize> {
        self.lengths
    }

    pub fn resolutions(&self) -> glm::TVec3<usize> {
        self.resolutions
    }

    pub fn ratios(&self) -> glm::Vec3 {
        self.ratios
    }

    pub fn as_cuboid(&self) -> Cuboid {
        Cuboid::new(self.lengths.x as f32, self.lengths.y as f32, self.lengths.z as f32)
    }
}

#[derive(Debug, Clone)]
pub struct Tool {
    csg_model: Arc<dyn Csg + Send + Sync>,
    position_offset: glm::Vec3,
}

impl Tool {
    pub fn new(csg_model: Arc<dyn Csg + Send + Sync>, position_offset: glm::Vec3) -> Tool {
        Tool { csg_model, position_offset }
    }

    pub fn csg_model(&self) -> &Arc<dyn Csg + Send + Sync> {
        &self.csg_model
    }

    pub fn position_offset(&self) -> glm::Vec3 {
        self.position_offset
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Command {
    Movement { target: glm::Vec3, duration: f32 },
    ToolCall { number: usize },
}
