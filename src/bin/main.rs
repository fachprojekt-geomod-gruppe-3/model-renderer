use std::{fs::File, io::{BufRead, BufReader}, sync::Arc, time::Instant, error, fmt};
use model_renderer::{prelude::*, camera::Direction as Dir, csg::{Cylinder, Axis}};
use luminance_glutin::{GlutinSurface, WindowDim, WindowOpt, CursorMode, GlutinError};
use glutin::{Event, WindowEvent, DeviceEvent, VirtualKeyCode as VKC, ElementState, MouseScrollDelta};
use nalgebra_glm as glm;
use anyhow::{Error, Context};

fn main() -> Result<(), Error> {
    let mut args = std::env::args().skip(1);

    let csv_filepath = args.next().ok_or(InitError::NotEnoughArguments)?;
    let csv_file = File::open(csv_filepath).context("Argument 1 is not a valid filepath.")?;

    let (x_length, x_resolution) = parse_dimension_arg(2, &mut args)?;
    let (y_length, y_resolution) = parse_dimension_arg(3, &mut args)?;
    let (z_length, z_resolution) = parse_dimension_arg(4, &mut args)?;

    if args.next().is_some() {
        return Err(Error::new(InitError::TooManyArguments));
    }

    eprintln!("Arguments parsed.");

    let workpiece = Workpiece::new(
        glm::vec3(x_length, y_length, z_length),
        glm::vec3(x_resolution, y_resolution, z_resolution),
    );

    let tools = vec![
        Tool::new(Arc::new(Cylinder::new(2.5, 30.0, Axis::Z)), glm::vec3(0.0, 0.0, 15.0)),
        Tool::new(Arc::new(Cylinder::new(1.0, 30.0, Axis::Z)), glm::vec3(0.0, 0.0, 15.0)),
    ];

    let commands = BufReader::new(csv_file).lines().enumerate().filter_map(|(i, line)| {
        let command_opt = parse_csv_line(line.ok()?);

        if command_opt.is_none() {
            eprintln!("Warning: Cannot parse line {} of input file.", i + 1);
        }

        command_opt
    });

    let window_options = WindowOpt::default().set_cursor_mode(CursorMode::Invisible);
    let surface = GlutinSurface::new(WindowDim::Fullscreen, "Machining Simulation", window_options)
        .map_err(InitError::GlutinError).context("Cannot create graphics surface.")?;

    eprintln!("Graphics surface created.");

    let renderer = VoxelRenderer::new(surface, workpiece, tools, commands)
        .map_err(InitError::GlutinError).context("Cannot create renderer.")?;
    
    main_loop(renderer, Camera::from_workpiece(workpiece));

    Ok(())
}

fn parse_dimension_arg<I>(n: u32, args: &mut I) -> Result<(usize, usize), Error>
where I: Iterator<Item = String> {
    let arg = args.next().ok_or(InitError::NotEnoughArguments)?;
    let ctx = || format!("Expected \"<number>[:<number>]\" at argument {}, found \"{}\".", n, arg);

    let mut split = arg.splitn(2, ':');
    let length = split.next().unwrap().parse().with_context(ctx)?;
    let resolution = match split.next() {
        Some(s) => s.parse().with_context(ctx)?,
        None => length,
    };
    
    Ok((length, resolution))
}

fn parse_csv_line(line: String) -> Option<Command> {
    let mut line_values = line.split(',').peekable();

    if let Some(&"tool") = line_values.peek() {
        line_values.nth(1)?.parse().map(|number| Command::ToolCall { number }).ok()
    } else {
        let mut numbers = line_values.filter_map(|s| s.parse().ok());
        let target = glm::vec3(numbers.next()?, numbers.next()?, numbers.next()?);
        let duration = numbers.next()?;

        Some(Command::Movement { target, duration })
    }
}

fn main_loop(mut renderer: VoxelRenderer, mut camera: Camera) {
    let initial_camera = camera;
    let mut state = ExecutionState::Pausing;

    match renderer.render(&camera) {
        Ok(()) => eprintln!("Render loop started."),
        Err(e) => eprintln!("Warning: Failed rendering a frame. Cause: {}", e),
    }

    let mut time = Instant::now();

    loop {
        match state {
            ExecutionState::Running => renderer.update_model(true),
            ExecutionState::Skipping => renderer.update_model(false),
            ExecutionState::Pausing => {},
            ExecutionState::Cancelled => break,
        }

        if time.elapsed().as_millis() > 16 {
            if let Err(e) = renderer.render(&camera) {
                eprintln!("Warning: Failed rendering a frame. Cause: {}", e);
            }

            time = Instant::now();
        }

        renderer.poll_surface_events(|event| {
            match event {
                Event::Suspended(true) => state.pause(),
                Event::WindowEvent { event, .. } => {
                    match event {
                        WindowEvent::CloseRequested | WindowEvent::Destroyed => state.cancel(),
                        WindowEvent::Focused(false) => state.pause(),
                        WindowEvent::KeyboardInput { input, .. } => {
                            if let Some(keycode) = input.virtual_keycode {
                                if input.state == ElementState::Released {
                                    match keycode {
                                        VKC::Escape => state.cancel(),
                                        VKC::Space => state.toggle_pause(),
                                        VKC::E => state.toggle_skip(),
                                        VKC::R => camera = initial_camera,
                                        _ => {},
                                    }
                                } else {
                                    match keycode {
                                        VKC::W | VKC::Up => camera.translate(1.5, Dir::Up),
                                        VKC::S | VKC::Down => camera.translate(1.5, Dir::Down),
                                        VKC::A | VKC::Left => camera.translate(1.5, Dir::Left),
                                        VKC::D | VKC::Right => camera.translate(1.5, Dir::Right),
                                        _ => {},
                                    }
                                }
                            }
                        },
                        WindowEvent::MouseWheel { delta, modifiers, .. } => {
                            if let MouseScrollDelta::LineDelta(_, v_delta) = delta {
                                if modifiers.shift {
                                    camera.zoom(0.05, v_delta);
                                } else {
                                    let dir = if v_delta > 0.0 { Dir::Forward } else { Dir::Backward };
                                    camera.translate(4.5, dir);
                                }
                            }
                        },
                        _ => {},
                    }
                },
                Event::DeviceEvent { event, .. } => {
                    if let DeviceEvent::MouseMotion { delta } = event {
                        camera.rotate(0.1, delta.0 as f32, delta.1 as f32);
                    }
                },
                _ => {},
            }
        });
    }

    drop(renderer);
    eprintln!("Render loop terminated.");
}

#[derive(Debug)]
enum InitError {
    TooManyArguments,
    NotEnoughArguments,
    GlutinError(GlutinError),
}

impl fmt::Display for InitError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InitError::TooManyArguments => write!(f, "Too many arguments."),
            InitError::NotEnoughArguments => write!(f, "Not enough arguments."),
            InitError::GlutinError(e) => e.fmt(f),
        }
    }
}

impl error::Error for InitError {}

impl From<GlutinError> for InitError {
    fn from(glutin_error: GlutinError) -> Self {
        InitError::GlutinError(glutin_error)
    }
}

#[derive(Debug, Clone, Copy)]
enum ExecutionState {
    Running,
    Skipping,
    Pausing,
    Cancelled,
}

impl ExecutionState {
    fn pause(&mut self) {
        *self = match self {
            ExecutionState::Cancelled => ExecutionState::Cancelled,
            _ => ExecutionState::Pausing,
        }
    }

    fn toggle_pause(&mut self) {
        *self = match self {
            ExecutionState::Running => ExecutionState::Pausing,
            ExecutionState::Skipping => ExecutionState::Pausing,
            ExecutionState::Pausing => ExecutionState::Running,
            ExecutionState::Cancelled => ExecutionState::Cancelled,
        }
    }

    fn toggle_skip(&mut self) {
        *self = match self {
            ExecutionState::Running => ExecutionState::Skipping,
            ExecutionState::Skipping => ExecutionState::Running,
            ExecutionState::Pausing => ExecutionState::Skipping,
            ExecutionState::Cancelled => ExecutionState::Cancelled,
        }
    }

    fn cancel(&mut self) {
        *self = ExecutionState::Cancelled;
    }
}
