use crate::machining::Workpiece;
use luminance_glutin::GlutinSurface;
use nalgebra_glm as glm;

#[derive(Debug, Clone, Copy)]
pub struct Camera {
    position: glm::Vec3,
    view_direction: glm::Vec3,
    yaw: f32,
    pitch: f32,
    fov: f32,
}

impl Camera {
    pub fn from_workpiece(workpiece: Workpiece) -> Camera {
        let yaw = 45.0;
        let pitch = -15.0;
        let view_direction = vec3_from_euler_angles(yaw, pitch);

        let xy = std::cmp::min(workpiece.lengths().x, workpiece.lengths().y) as f32;
        let z = glm::root_two::<f32>() * xy / 75f32.to_radians().tan();
        let position = glm::vec3(-xy, -xy, z);

        Camera {
            position,
            view_direction,
            yaw,
            pitch,
            fov: glm::quarter_pi(),
        }
    }

    pub fn translate(&mut self, speed: f32, direction: Direction) {
        let right_vector = glm::normalize(&glm::cross(&self.view_direction, &glm::vec3(0.0, 0.0, 1.0)));
        let up_vector = glm::normalize(&glm::cross(&right_vector, &self.view_direction));

        self.position += speed * match direction {
            Direction::Right => right_vector,
            Direction::Left => -right_vector,
            Direction::Up => up_vector,
            Direction::Down => -up_vector,
            Direction::Forward => self.view_direction,
            Direction::Backward => -self.view_direction
        };
    }

    pub fn rotate(&mut self, speed: f32, x_offset: f32, y_offset: f32) {
        self.yaw += speed * x_offset;
        self.pitch += speed * y_offset;

        if self.pitch.abs() > 89.0 {
            self.pitch = 89_f32.copysign(self.pitch)
        }

        self.view_direction = vec3_from_euler_angles(self.yaw, self.pitch);
    }

    pub fn zoom(&mut self, speed: f32, delta: f32) {
        self.fov -= speed.copysign(delta);

        if self.fov < 0.1 {
            self.fov = 0.1;
        } else if self.fov > glm::half_pi() {
            self.fov = glm::half_pi();
        }
    }

    pub fn projection(&self, surface: &GlutinSurface) -> glm::Mat4 {
        let [width, height] = surface.size();

        glm::perspective(width as f32 / height as f32, self.fov, 1.0, 1000.0)
    }

    pub fn view(&self) -> glm::Mat4 {
        glm::look_at(&self.position, &(self.position + self.view_direction), &glm::vec3(0.0, 0.0, 1.0))
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Direction {
    Right,
    Left,
    Up,
    Down,
    Forward,
    Backward,
}

fn vec3_from_euler_angles(yaw: f32, pitch: f32) -> glm::Vec3 {
    glm::normalize(&glm::vec3(
        pitch.to_radians().cos() * yaw.to_radians().cos(),
        pitch.to_radians().cos() * yaw.to_radians().sin(),
        pitch.to_radians().sin(),
    ))
}
